Formularios 
- Películas:
    * Nombre 
    * Año
    * Trama
    * Trailer
    * Compañía
    * Presupuesto
- Personas:
    * Nombre 
    * Fecha de nacimiento
    * Puesto
- Super Persona (Web Service)
    * (SELECT) Actor
    * (SELECT)Heroe/Villano(WS)
    * (SELECT) Película
- Calificaciones
    * (SELECT) Película
    * (INPUT) Actor
    * (INPUT) Heroe
- Analitics
    * (SELECT) Película
    * (SELECT) Heroe { V }
    * (SELECT) Villanos { S }
    * Ganador
    * Número Aparición en Películas
